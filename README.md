# Portfolio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.17.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


//Process of creation:
npm install bootstrap --save
npm i font-awesome --save
npm i magnific-popup --save
npm i jquery-nice-select --save


ng generate component header
ng generate module home
ng generate component home/home --spec=false
ng generate component home/banner --spec=false
ng generate component home/about --spec=false
ng generate component home/brand --spec=false
ng generate component home/services --spec=false
ng generate component home/portfolio --spec=false
ng generate component home/testimonial --spec=false
ng generate component home/newsletter --spec=false
ng generate component footer

npm install jquery popper.js --save
npm i owl.carousel --save

ng generate class Contact


ng generate module inner
ng generate component inner/inner --spec=false
ng generate component inner/innerBanner --spec=false
ng generate component inner/innerAbout --spec=false

ng generate component contactUs