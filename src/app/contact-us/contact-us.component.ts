import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  title = 'Contact Us';
  constructor() { }

  //Form object
  contactForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl(''),
    subject: new FormControl(''),
    messege: new FormControl(''),
  });
  

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.contactForm.value);
  }

  reset() {
    this.contactForm.reset();
  }

  ngOnInit() {
    //To set default value
    this.contactForm.patchValue({name: 'Default Name', email: 'test@mail.com'});
    //Another option to set default value
    this.contactForm.get('name').setValue('Sufiyan Malek');
  }

}
