import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home/home.component";
import { InnerAboutComponent } from "./inner/inner-about/inner-about.component";
import { ServicesComponent } from "./home/services/services.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";

const routes: Routes = [
  {path: "", pathMatch: "full", component: HomeComponent},
  {path: "about", component: InnerAboutComponent},
  {path: "services", component: ServicesComponent},
  {path: "contact", component: ContactUsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
