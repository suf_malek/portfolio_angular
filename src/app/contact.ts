export class Contact {

    constructor(
        public id: number,
        public email: string,
        public subject: string,
        public name: string,
        public messege: string
    ) {}
}
