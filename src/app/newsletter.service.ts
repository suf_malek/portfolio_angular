import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsletterService {

  constructor() { }
  newsletter = [
    {id: 1, email: "c001@email.com"},
    {id: 2, email: "c002@email.com"},
    {id: 3, email: "c003@email.com"}
  ];

  public getNewsletter() {
    return this.newsletter;
  }

  public createNewsletter(newsletter: {id, email}) {
    this.newsletter.push(newsletter);
    console.warn(newsletter);
  }

}
