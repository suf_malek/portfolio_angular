import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inner-about',
  templateUrl: './inner-about.component.html',
  styleUrls: ['./inner-about.component.css']
})
export class InnerAboutComponent implements OnInit {

  title = 'About Us';
  constructor() { }

  ngOnInit() {
  }

}
