import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InnerComponent } from './inner/inner.component';
import { InnerBannerComponent } from './inner-banner/inner-banner.component';
import { InnerAboutComponent } from './inner-about/inner-about.component';
import { HomeModule } from '../home/home.module';


@NgModule({
  declarations: [InnerComponent, InnerBannerComponent, InnerAboutComponent ],
  imports: [
    CommonModule,
    HomeModule
  ],
  exports: [
    InnerBannerComponent
  ]
})
export class InnerModule { }
