import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inner-banner',
  templateUrl: './inner-banner.component.html',
  styleUrls: ['./inner-banner.component.css']
})
export class InnerBannerComponent implements OnInit {

  @Input() title:string = "Home";

  constructor() { }

  ngOnInit() {
  }

}
