import { Component, OnInit } from '@angular/core';
import { Newsletter } from 'src/app/newsletter';
import { NewsletterService } from "../../newsletter.service";

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {

  constructor(public newsletterService: NewsletterService) { }

  submitted = false;

  newsletter = {id: null, email: ""};
  newsletterNew = new Newsletter(null,"");

  createNewsletter() {
    console.log(this.newsletter);
    this.newsletterService.createNewsletter(this.newsletter);
    this.newsletter = {id: null, email: ""};
  }

  onSubmit() { this.submitted = true; }

  get diagnostic() { return JSON.stringify(this.newsletter); }

  ngOnInit() {
  }

}
