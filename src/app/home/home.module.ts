import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { BrandComponent } from './brand/brand.component';
import { ServicesComponent } from './services/services.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { FormsModule }   from '@angular/forms';



@NgModule({
  declarations: [BannerComponent, AboutComponent, HomeComponent, BrandComponent, ServicesComponent, PortfolioComponent, TestimonialComponent, NewsletterComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    AboutComponent
  ]
})
export class HomeModule { }
