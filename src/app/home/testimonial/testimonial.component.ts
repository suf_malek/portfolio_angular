import { Component, OnInit, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css']
})
export class TestimonialComponent implements OnInit {

  constructor(private elRef:ElementRef) {
    //this.elRef.nativeElement
  }

  ngOnInit() {
    //var el = this.elRef.nativeElement;
  }

  
  ngAfterContentInit(): void {
    var className = document.getElementsByClassName('testi_slider');
    console.log(this.elRef.nativeElement);
    //$(this.elRef.nativeElement).owlCarousel();
    //var div = this.elRef.nativeElement.querySelector('.testi_slider');
    //console.log(div);
    $(className).owlCarousel({
      loop: true,
      margin: 30,
      items: 2,
      autoplay: true,
      smartSpeed: 2500,
      dots: true,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1
        },
        991: {
          items: 2
        }
      }
    });
    //$(this.elRef.nativeElement).owlCarousel();
  }
  
}
